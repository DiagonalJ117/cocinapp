import React, {Component,PureComponent} from 'react';
import {View,Text,StyleSheet,TouchableHighlight,Image,Dimensions,ScrollView,Button,TouchableOpacity, FlatList} from 'react-native';
import Carousel from "react-native-snap-carousel";
import {Searchbar, FAB, Dialog, Portal, Provider, TextInput, Card, Title} from "react-native-paper";

const itemWidth = 100;
const Realm = require('realm');

const RecetaSchema = {
  name: 'Receta',
  primaryKey: 'id',
  properties: {
    id: 'int',
    nombre: 'string',
    descripcion: 'string',
    ingredientes: 'string',
    pasosPrep: 'string',
    tipo: 'string'
  }
};

let realm = new Realm({schema: [RecetaSchema]})

class recetario extends Component{
  static navigationOptions={
   title:'Recetas',
    
  }

  

  render() {

    const { navigate } = this.props.navigation; 
    console.log(this.props, "props is here");


    return (
          
      <ScrollView>
 
 <View style={styles.container}>

 <View style={styles.cuadro1}>         

 </View> 


        <View style={styles.cuadro1}>
        <FlatList
  data={realm.objects('Receta')}
  renderItem={({ item }) => (
    <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
      <TouchableOpacity onPress={() => navigate("SegundaPantalla", { 
        comida: item.nombre,
        ingreds: item.ingredientes,
        pasos: item.pasosPrep
        })}>
      <Card elevation={10}>
        
        <Card.Content style={{alignItems: 'center'}}>
        <Title>{item.nombre}</Title>
        
      </Card.Content>
      <Card.Cover source= {require('./restaurant.png')}></Card.Cover>
      </Card>
      </TouchableOpacity>
      
    </View>
  )}
  //Setting the number of column
  numColumns={3}
  keyExtractor={(item, index) => index.toString()}
/>         
        </View> 

</View>
      </ScrollView>
      
   
    );
  } 
}

const styles=StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'white'
  },
  
    imagen :{  
  width : 150,    
  height : 120,
  marginBottom:5,
   marginLeft:2 
  },   
 
  texto: {    
fontSize : 14,    
color : 'black',     
textAlign: 'center', 
marginTop:10,
fontFamily:'Calibri Light',
},
  
  
  cuadro1 :{
  flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,      
  },
  cuadro2 :{
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginEnd:20
  },
   buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#039be5',
  }
   
 
  
});
export default recetario;

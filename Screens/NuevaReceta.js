import React, { Component } from 'react';
import { View, TextInput, Text, StyleSheet, TouchableHighlight, Image, CheckBox,Button,SafeAreaView,Alert,ScrollView} from 'react-native';
import Constants from 'expo-constants';




class NuevaReceta extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title:'Nueva receta',
    };
  };




  render() {
    const { state, navigate } = this.props.navigation;

    
    return (

        <ScrollView>

      <View style={styles.container}>



<div>Nombre de la nueva receta
</div>

<TextInput
style={{height:20,width:300,borderColor:'gray',borderWidth:1}}
/>


<div style={{marginTop:10}}> Ingredientes
</div>

<TextInput multiline
style={{height:80,width:300,borderColor:'gray',borderWidth:1}}
/>

<div style={{marginTop:10}}> Preparación
</div>

<TextInput multiline
style={{height:80,width:300,borderColor:'gray',borderWidth:1}}
/>


<div style={{marginTop:10}}>Inserta una imágen (opcional)
</div>
      
<Image style ={ styles.imagenlogo} source = {require('./restaurant.png')}/>



<div>
</div>


<div>
</div>

<Button
title="Guardar"
/>

      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
    imagenlogo :{  
  width :300,    
  height : 200,
  marginBottom:20,
  marginTop:10  
    },  
 
 
});

export default NuevaReceta;
import React, {Component,PureComponent} from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image, CheckBox,ScrollView,TouchableOpacity,Dimensions} from 'react-native';
import Constants from 'expo-constants';
import { SearchBar } from 'react-native-elements';


export default class Favoritos extends React.Component {
   state = {
    search: '',
  };
  updateSearch = search => {
    this.setState({ search });
  };
  render() {
     const { search } = this.state;
       const { navigate } = this.props.navigation; 
    console.log(this.props, "props is here");
    return (


<ScrollView>
      <View style={styles.container}> 
      <Text style={styles.paragraph}> Favoritos </Text>
     

     <SearchBar
        placeholder="Buscar Platillo"
        onChangeText={this.updateSearch}
        value={search}/>
        
        <br/>
        <View style ={{flexDirection: 'row'}}>
        <TouchableHighlight onPress={() =>navigate("SegundaPantalla")}>
              <Image style ={ styles.imagen} source = {require('./desayuno1.jpg')}/>
              </TouchableHighlight>    
       <Text style = {styles.texto}>Carne</Text>
     </View>
       <br/>     
       <View style ={{flexDirection: 'row'}}>
        <TouchableHighlight onPress={() =>navigate("SegundaPantalla")}>
              <Image style ={ styles.imagen} source = {require('./desayuno1.jpg')}/>
              </TouchableHighlight> 
           <Text style = {styles.texto}>Chile relleno</Text>
    </View>
        <br/> 
        <View style ={{flexDirection: 'row'}}>
        <TouchableHighlight onPress={() =>navigate("SegundaPantalla")}>
              <Image style ={ styles.imagen} source = {require('./desayuno1.jpg')}/>
              </TouchableHighlight> 
         <Text style = {styles.texto}>Sopa de tortilla</Text>
          </View>
        <br/> 
     <View style ={{flexDirection: 'row'}}>
      <TouchableHighlight onPress={() =>navigate("SegundaPantalla")}>
              <Image style ={ styles.imagen} source = {require('./desayuno1.jpg')}/>
              </TouchableHighlight> 
         <Text style = {styles.texto}>Tacos de carne asada</Text>
          </View>
        <br/> 
        
        
        </View>
                 

</ScrollView>


    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: 'white',
    padding: 8,
    
  },
  paragraph: {
    margin: 8,
    fontSize: 30,
    fontWeight: 'normal',
    textAlign: 'center',
  },
imagen :{    
width : 100,    
height : 100,  
},

texto: {    
fontSize : 20,    
color : 'black',     
textAlign: 'center', 
padding:20   
},
}); 

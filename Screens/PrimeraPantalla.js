import React, {Component,PureComponent} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,Image,Dimensions,ScrollView, Button, Picker, FlatList} from 'react-native';
import Carousel from "react-native-snap-carousel";
import {Searchbar, FAB, Dialog, Portal, Provider, TextInput, Card, Title} from "react-native-paper";

const itemWidth = 100;

const Realm = require('realm');

const RecetaSchema = {
  name: 'Receta',
  primaryKey: 'id',
  properties: {
    id: 'int',
    nombre: 'string',
    descripcion: 'string',
    ingredientes: 'string',
    pasosPrep: 'string',
    tipo: 'string'
  }
};

let realm = new Realm({schema: [RecetaSchema]})

class PrimeraPantalla extends Component{
  static navigationOptions={ 
    title: 'Lista de Recetas',
    visibleDialog: false 
  }
  constructor(props){
    super(props);
  this.state= {
    fquery:'',
    newRecetaName:'',
    newRecetaIng: '',
    newRecetaDesc:'Sin Descripcion',
    newRecetaPasos: '',
    newRecetaTipo: 'desayuno',
    desayunosHolder: [],
    comidasHolder: [],
    cenasHolder: [],
  }

}

componentDidMount() {
  this.setState({desayunosHolder: realm.objects('Receta').filtered("tipo ="+ "'desayuno'")});
  this.setState({comidasHolder: realm.objects('Receta').filtered("tipo ="+ "'comida'")});
  this.setState({cenasHolder: realm.objects('Receta').filtered("tipo ="+ "'cena'")});
}

searchFilterFunction = text => {     
  if(text.length > 0) {
    this.setState({ desayunosHolder: realm.objects('Receta').filtered("tipo ="+ "'desayuno'").filtered("ingredientes LIKE[c] "+ "'*"+text+"*'")});  
    this.setState({ comidasHolder: realm.objects('Receta').filtered("tipo ="+ "'comida'").filtered("ingredientes LIKE[c] "+ "'*"+text+"*'") });  
    this.setState({ cenasHolder: realm.objects('Receta').filtered("tipo ="+ "'cena'").filtered("ingredientes LIKE[c] "+ "'*"+text+"*'")});  
  }else{
    this.setState({desayunosHolder: realm.objects('Receta').filtered("tipo ="+ "'desayuno'")});
    this.setState({comidasHolder: realm.objects('Receta').filtered("tipo ="+ "'comida'")});
    this.setState({cenasHolder: realm.objects('Receta').filtered("tipo ="+ "'cena'")});
  }
};

GenerateRandomNumber = () => {

  var RandomNumber = Math.floor(Math.random() * 1000000) + 1;

  return RandomNumber;
}
  _showAddDialog = () => {
    this.setState({ newRecetaName: ''});
    this.setState({ newRecetaIng: ''});
    this.setState({ newRecetaPasos: ''});
    this.setState({newRecetaTipo: 'desayuno'});
    this.setState({ visibleDialog: true });
  }
  _hideAddDialog = () => this.setState({ visibleDialog: false });
  _addReceta = () => {
    console.warn(this.state.newRecetaIng.split(','));
    realm.write(() => {
      realm.create('Receta', {
        id: this.GenerateRandomNumber(),
        nombre: this.state.newRecetaName,
        descripcion: this.state.newRecetaDesc,
        ingredientes: this.state.newRecetaIng,
        pasosPrep: this.state.newRecetaPasos,
        tipo: this.state.newRecetaTipo
      });
    });
    console.warn(realm.objects('Receta'));
    this.setState({visibleDialog: false});
    this.setState({desayunosHolder: realm.objects('Receta').filtered("tipo ="+ "'desayuno'")});
    this.setState({comidasHolder: realm.objects('Receta').filtered("tipo ="+ "'comida'")});
    this.setState({cenasHolder: realm.objects('Receta').filtered("tipo ="+ "'cena'")});
  };
  render() {
    const { fquery, visibleDialog } = this.state;
    const { navigate } = this.props.navigation;
    console.log(this.props, "props is here");



    return (
      <Provider>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>    
      <View style={styles.container}>
 
 <View style={styles.cuadro1}>         
         
<Searchbar
placeholder="Buscar ingredientes"
lightTheme
round
onChangeText={text => this.searchFilterFunction(text)}
/>
 </View> 

<View>

<Text style = {styles.texto}>DESAYUNOS</Text>
        <View style={styles.cuadro11}>
        <FlatList
  data={this.state.desayunosHolder}
  renderItem={({ item }) => (
    <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
      <TouchableOpacity onPress={() => navigate("SegundaPantalla", { 
        comida: item.nombre,
        ingreds: item.ingredientes,
        pasos: item.pasosPrep
        })}>
      <Card elevation={10}>
        
        <Card.Content style={{alignItems: 'center'}}>
        <Title>{item.nombre}</Title>
        
      </Card.Content>
      <Card.Cover source= {require('./restaurant.png')}></Card.Cover>
      </Card>
      </TouchableOpacity>
      
    </View>
  )}
  //Setting the number of column
  numColumns={3}
  keyExtractor={(item, index) => index.toString()}
/>


        
        </View> 

<Text style = {styles.texto}>COMIDAS</Text>
<View style={styles.cuadro11}>
        <FlatList
  data={this.state.comidasHolder}
  renderItem={({ item }) => (
    <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
      <TouchableOpacity onPress={() => navigate("SegundaPantalla", { 
        comida: item.nombre,
        ingreds: item.ingredientes,
        pasos: item.pasosPrep
        })}>
      <Card elevation={10}>
        
        <Card.Content style={{alignItems: 'center'}}>
        <Title>{item.nombre}</Title>
        
      </Card.Content>
      <Card.Cover style={styles.imagen} source= {require('./restaurant.png')}></Card.Cover>
      </Card>
      </TouchableOpacity>
      
    </View>
  )}
  //Setting the number of column
  numColumns={3}
  keyExtractor={(item, index) => index.toString()}
/>


        
        </View>  

<Text style = {styles.texto}>CENAS</Text>
<View style={styles.cuadro11}>
        <FlatList
  data={this.state.cenasHolder}
  renderItem={({ item }) => (
    <View style={{ flex: 1, flexDirection: 'column', margin: 10 }}>
      <TouchableOpacity onPress={() => navigate("SegundaPantalla", { 
        comida: item.nombre,
        ingreds: item.ingredientes,
        pasos: item.pasosPrep
        })}>
      <Card elevation={10}>
        
        <Card.Content style={{alignItems: 'center'}}>
        <Title>{item.nombre}</Title>
        
      </Card.Content>
      <Card.Cover style={{resizeMode: 'cover'}} source= {require('./restaurant.png')}></Card.Cover>
      </Card>
      </TouchableOpacity>
    </View>
  )}WTF
  //Setting the number of column
  numColumns={3}
  keyExtractor={(item, index) => index.toString()}
/>

</View>
        </View> 
          <Portal>
            <Dialog visible={visibleDialog} onDismiss={this._hideAddDialog}>
              <Dialog.Title>Agregar Receta</Dialog.Title>
              <Dialog.Content>
                <TextInput label="Nombre"
                onChangeText={(value) => this.setState({ newRecetaName: value})}
                value={this.state.newRecetaName}
                ></TextInput>
                <TextInput multiline label="Ingredientes (separar por coma)"
                onChangeText={(value) => this.setState({ newRecetaIng: value})}
                value={this.state.newRecetaIng}
                ></TextInput>
                <TextInput multiline label="Pasos para Preparar (separar por coma)"
                onChangeText={(value) => this.setState({ newRecetaPasos: value})}
                value={this.state.newRecetaPasos}
                ></TextInput>
            <Picker
            selectedValue={this.state.newRecetaTipo}
            onValueChange={(itemValue, itemIndex) => {
              
              this.setState({newRecetaTipo: itemValue})
              console.warn(this.state.newRecetaTipo)
            }}
            >
              <Picker.Item label="Desayuno" value="desayuno"/>
              <Picker.Item label="Comida" value="comida"/>
              <Picker.Item label="Cena" value="cena"/>
            </Picker>
              </Dialog.Content>
              <Dialog.Actions style={{alignContent:"flex-start"}}>
              <Button title="Agregar" onPress={this._addReceta}></Button>
            </Dialog.Actions>
            </Dialog>
          </Portal>
        

           
      </View>

      
   </ScrollView>

<Portal>
<FAB
style={styles.fab}
small
icon="add"
onPress={() => this._showAddDialog()}
/>

</Portal>
</Provider>
    );
  } 
}

const styles=StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor:'white'
  },
  
    imagen :{  
  width : 150,    
  height : 120,
  marginBottom:10,
   marginLeft:2 
  },   
 
  texto: {    
fontSize : 14,    
color : 'black',     
textAlign: 'center', 
padding:12,
fontFamily:'Calibri Light',
},
  
  
  cuadro1 :{
  flexDirection: 'row',
  alignItems: 'center',
    
  },
  cuadro11 :{
    flexDirection: 'row',
    alignSelf:'center',
    padding: 10
    },
  cuadro2 :{
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginEnd:20
  },
  fab: {
    position: 'absolute',
    backgroundColor: 'limegreen',
    margin: 16,
    right: 0,
    bottom: 0,
  },
   
 
  
});
export default PrimeraPantalla;

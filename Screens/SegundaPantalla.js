import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image, CheckBox,ScrollView, FlatList} from 'react-native';
import { Card } from 'react-native-elements';

function Paso({ num, content }) {
  return (
    
      <Card>
      <Text>{num} {content}</Text>
      </Card>
    
  );
}

function Ingrediente({ num, content }) {
  return (
    
      <Text>{num} - {content}</Text>
    
  );
}

class SegundaPantalla extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title:'Ingredientes y preparación',
    };
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={styles.dividerStyle}
      />
    );
  }



  render() {
    const {  navigate } = this.props.navigation;
    this.state = {
      nombreComida: '',
      cantidadPersonas: '',
      ingredientes: [
        { num: 1, desc:'6 tortillas de trigo o de maíz'},
        { num: 2, desc:'350g de carne picada de ternera o de pollo'},
        { num: 3, desc: '4 dientes de ajo'},
        { num: 4, desc:'500g de tomates'},
        { num:5, desc:'sal y pimienta'},
        { num:6, desc:'aceite'},
        { num:7, desc: 'OPCIONAL: Queso rallado'}
      ],
      pasosReceta: [
        { num: 1, desc: 'Para comenzar con nuestros tacos mexicanos, picamos los ajos y los cocinamos a fuego muy suave durante 1 minuto. Que no coja color'},
        { num: 2, desc: 'Añadimos el medio kilo de tomates, ya pelados y troceados y subimos la potencia del fuego. Salpimentamos y cocinamos durante un rato. El tomate soltará agua, debe evaporarse toda que, puede tartar un rato. Una vez evaporada dejamos cocinar a fuego suave mientras removemos constantemente, durante 5 minutos más, para que coja buen sabor'}
      ],

    };
    return (
      <ScrollView style={{marginBottom: 25}}>
      <View style={styles.container}>
<Text style = {styles.titulo}>{this.props.navigation.getParam('comida')}</Text>

<Card>
  
  <Image style ={ styles.imagenlogo} source = {require('./restaurant.png')}/>
  
</Card>

<Card title="Ingredientes">

  <FlatList
    data={this.props.navigation.getParam('ingreds').split(',')}
    renderItem = {({item, index}) => <Ingrediente num={index+1} content={item}></Ingrediente>}
  >
  </FlatList>

</Card>

<Card title="Preparación">


  <FlatList
    data={this.props.navigation.getParam('pasos').split(',')}
    renderItem = {({item, index}) => <Paso num={index+1} content={item}></Paso>}
  >
    
  </FlatList>
</Card>

      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    alignItems: 'stretch',
    backgroundColor: 'white',
    paddingBottom:20
    
  },
    imagenlogo :{  
  alignSelf:"center",
  width :300,    
  height : 200,
  marginBottom:20,
  marginTop:10  
  },
  titulo:{
    alignSelf:"center",
    fontSize: 40,
  
  },
  subtitulo: {
    fontSize: 20,
  }   
 
 
});

export default SegundaPantalla;

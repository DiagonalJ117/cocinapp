import React, {Component} from 'react';
import {DrawerNavigator,StackNavigator} from 'react-navigation';
import {View} from 'react-native';




import Login from './Screens/Login';
import PrimeraPantalla from './Screens/PrimeraPantalla';
import SegundaPantalla from './Screens/SegundaPantalla';
import Tercerapantalla from './Screens/Tercerapantalla';
/*import Cuartapantalla from './Screens/CuartaPantalla';*/
import Favoritos from './Screens/Favoritos';
import Recetario from './Screens/recetario';
import NuevaReceta from './Screens/NuevaReceta';


class App extends Component 
{
  render()
  {
      return(
        <View style={{flex:1,backgroundColor:'transparent'}}>
        <AppDrawerNavigator/>
        </View>
      )
      

  }
}

/*menu para todas las pantallas de la app*/
const AppStackNavigator = StackNavigator(
{
  Login:{screen: Login},
  PrimeraPantalla:{screen: PrimeraPantalla},
  SegundaPantalla:{screen: SegundaPantalla},
  Favoritos:{screen:Favoritos},
  Recetario:{screen: Recetario},
  NuevaReceta:{screen: NuevaReceta},
  Tercerapantalla:{screen: Tercerapantalla},

 
})

/*menu para el drawer*/
const AppDrawerNavigator=DrawerNavigator({

  Cocinapp:AppStackNavigator,
  /*Login:{screen: Login},*/
  Home:{screen: PrimeraPantalla},
  /*SegundaPantalla:{screen: SegundaPantalla},*/
  Favoritos:{screen:Favoritos},
  Recetario:{screen: Recetario}
})


export default App;


